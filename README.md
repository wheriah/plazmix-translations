<div align="center">

 # Plazmix Network :: Translations

 <a href="https://gitlab.com/itzstonlex/plazmix-translations/-/blob/60a6c3ff3615a7fcded2b1353d8ae867c02c21da/LICENSE">
   <img src="https://img.shields.io/github/license/whilein/nmslib">
 </a>

 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **Plazmix Network (mc.plazmix.net)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[Discord](https://plazmix.xyz/discord)**
* **[ВКонтакте](https://vk.me/plazmixnetwork)**
